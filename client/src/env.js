export const env = 'production';
let envUrl = 'http://localhost:5000'; // Dev Server URL
let _socketUrl = 'http://localhost:5001';

if (env === 'production') {
  envUrl = 'http://localhost:5000'; // Production Server URL
  _socketUrl = 'http://localhost:5001';
}

export const isDevelopment = (env === 'development');
export const isProduction = (env === 'production');
export const apiURL = envUrl;
export const socketUrl = _socketUrl;
