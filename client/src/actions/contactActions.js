import { APIAuthGet } from '../lib/request';

import {
  CONTACTS_FETCHED,
  FETCHING_CONTACTS
} from './types';


export const fetchContacts = () => {
  return (dispatch, getState) => {
      const {
        currentUser
      } = getState();
      dispatch({ type: FETCHING_CONTACTS });
      APIAuthGet(currentUser.session, '/chat/friends')
      .then(({ data }) => {
        dispatch(updateContacts(data));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const updateContacts = (threads) => {
    return (dispatch) => {
      dispatch({ type: CONTACTS_FETCHED, payload: threads });
    };
};
