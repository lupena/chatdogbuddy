import { APIAuthGet, APIAuthPost } from '../lib/request';

import {
  THREADS_FETCHED,
  MESSAGES_FETCHED
} from './types';


export const fetchThreads = () => {
  return (dispatch, getState) => {
      const {
        currentUser
      } = getState();
      APIAuthGet(currentUser.session, '/chat')
      .then(({ data }) => {
        dispatch(updateThreads(data.threads));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const updateThreads = (threads) => {
    return (dispatch) => {
      dispatch({ type: THREADS_FETCHED, payload: threads });
    };
};

export const fetchMessages = (recipientId) => {
  return (dispatch, getState) => {
      const {
        currentUser
      } = getState();
      APIAuthGet(currentUser.session, `/chat/${recipientId}`)
      .then(({ data }) => {
        dispatch(updateMessages(data));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};

export const updateMessages = (messages) => {
    return (dispatch) => {
      dispatch({ type: MESSAGES_FETCHED, payload: messages });
    };
};

export const submitMessage = (recipientId, message) => {
  return (dispatch, getState) => {
      const {
        currentUser
      } = getState();
      const payload = { recipientId, message };
      APIAuthPost(currentUser.session, payload, `/chat/${recipientId}`)
      .then(() => {
        dispatch(fetchMessages(recipientId));
      })
      .catch((error) => {
        console.log(error);
      });
  };
};
