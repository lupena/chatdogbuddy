import swal from 'sweetalert';
import { APIpost } from '../lib/request';


import {
  USER_LOGIN_IN_PROCESS,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT
} from './types';

export const loginAccount = (username, password, onSuccess) => {
  return (dispatch) => {
    dispatch({ type: USER_LOGIN_IN_PROCESS });
      APIpost({
        email: username,
        password
      }, '/auth/login')
      .then(({ data }) => {
        dispatch(loginSuccessful({ ...data.user, token: data.token }));
        onSuccess();
      })
      .catch((error) => {
        dispatch({ type: USER_LOGOUT });
        swal('Upss...', 'Username and password does not match.', 'error');
        console.log(error);
      });
  };
};

export const loginSuccessful = (user) => ({
  type: USER_LOGIN_SUCCESS, payload: user
});

export const signupAccount = (user, onSuccess) => {
  return (dispatch) => {
    APIpost(user, '/auth/signup')
      .then(({ data }) => {
        if (data.name === 'ValidationError') {
          swal('Oops!', data.message, 'error');
          return;
        }
        if ('errmsg' in data) {
          swal('Oops!', data.errmsg, 'error');
          return;
        }
        dispatch(loginSuccessful({ ...data.user, token: data.token }));
        onSuccess();
      })
      .catch(({ response }) => {
        swal('Upss...', response.data.error, 'error');
      });
  };
};


export const logoutAccount = () => {
  return (dispatch) => {
    dispatch({ type: USER_LOGOUT });
  };
};
