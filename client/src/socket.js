import openSocket from 'socket.io-client';
import { apiURL } from './env.js';

const socket = openSocket(apiURL);
function subscribeToThread(threadId) {
  socket.emit('enter thread', threadId);
}

function unSubscribeToThread(threadId) {
  socket.emit('leave thread', threadId);
}

function newMessageSent(threadId) {
  socket.emit('new message', threadId);
}

function onNewMessageReceived(cb) {
  socket.on('new message', cb);
}


export { subscribeToThread, unSubscribeToThread, newMessageSent, onNewMessageReceived };
