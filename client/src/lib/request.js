import axios from 'axios';
import { apiURL } from '../env.js';

const getHeadersConfig = (session) => {
  const headers = {
    'Content-Type': 'application/json',
    Authorization: session.token
  };
  return headers;
};

export const APIget = (endpoint, params = '') => {
  return axios({
    baseURL: apiURL,
    url: endpoint,
    params,
    method: 'get'
  });
};

export const APIpost = (data, endpoint) => {
  const url = `${apiURL}${endpoint}`;
  return axios.post(url, data);
};

export const APIAuthPost = (session, data, endpoint) => {
  const computedHeaders = getHeadersConfig(session);
  const url = `${apiURL}${endpoint}`;
  return axios.post(url, data, { headers: computedHeaders });
};

export const APIAuthGet = (session, endpoint, params = '') => {
  const headers = getHeadersConfig(session);
  return axios({
    baseURL: apiURL,
    url: endpoint,
    params,
    method: 'get',
    headers
  });
};

export const getJson = (Uri) => {
  const headers = getHeadersConfig();
  return axios({
    url: Uri,
    method: 'get',
    headers
  });
};
