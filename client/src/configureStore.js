import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import reducers from './reducers';

const persistConfig = {
  key: 'root',
  storage,
};

export default function configureStore() {
  const persistedReducer = persistReducer(persistConfig, reducers);
  const store = createStore(
   persistedReducer, /* preloadedState, */
   window.initialState,
   compose(
    applyMiddleware(ReduxThunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
  );
  const persistor = persistStore(store);
  return { store, persistor };
}
