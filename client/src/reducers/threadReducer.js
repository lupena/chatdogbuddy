import {
        THREADS_FETCHED,
        FETCHING_THREADS,
        MESSAGES_FETCHED,
        USER_LOGOUT
       } from '../actions/types';

const INTIAL_STATE = {
                        list: [],
                        activeThread: {
                          messages: []
                        },
                        meta: {
                          isLoading: false
                        }
                      };

export default function (state = INTIAL_STATE, action) {
  if (action.error) {
    return state;
  }
  switch (action.type) {
    case THREADS_FETCHED:
      return {
        ...state,
        list: action.payload,
        meta: { isLoading: false }
      };
    case MESSAGES_FETCHED:
      return {
        ...state,
        activeThread: action.payload,
        meta: { isLoading: false }
      };
    case FETCHING_THREADS:
      return {
        ...state,
        meta: { isLoading: true }
      };
    case USER_LOGOUT:
      return INTIAL_STATE;
    default: {
      return state;
    }
  }
}
