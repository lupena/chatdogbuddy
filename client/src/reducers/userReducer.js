import {
        USER_LOGIN_IN_PROCESS,
        USER_LOGIN_SUCCESS,
        USER_LOGOUT
       } from '../actions/types';

const INTIAL_STATE = {
                        isAuth: false,
                        _id: null,
                        email: null,
                        meta: {
                          isLoading: false
                        },
                        session: null
                      };

export default function (state = INTIAL_STATE, action) {
  if (action.error) {
    return state;
  }
  switch (action.type) {
    case USER_LOGIN_IN_PROCESS:
      return {
        ...state,
        meta: {
          isLoading: true
        }
      };
    case USER_LOGIN_SUCCESS: {
    const { _id, email, token } = action.payload;
      return {
        ...state,
        isAuth: true,
        _id,
        email,
        meta: {
          isLoading: false
        },
        session: { token }
      };
    }
    case USER_LOGOUT:
      return INTIAL_STATE;
    default: {
      return state;
    }
  }
}
