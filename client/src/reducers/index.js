import { combineReducers } from 'redux';
import userReducer from './userReducer.js';
import contactsReducer from './contactsReducer.js';
import threadReducer from './threadReducer.js';


const rootReducer = combineReducers({
  currentUser: userReducer,
  contacts: contactsReducer,
  threads: threadReducer
});

export default rootReducer;
