import {
        CONTACTS_FETCHED,
        FETCHING_CONTACTS,
        USER_LOGOUT
       } from '../actions/types';

const INTIAL_STATE = {
                        list: [],
                        meta: {
                          isLoading: false
                        }
                      };

export default function (state = INTIAL_STATE, action) {
  if (action.error) {
    return state;
  }
  switch (action.type) {
    case CONTACTS_FETCHED:
      return {
        ...state,
        list: action.payload,
        meta: { isLoading: false }
      };
    case FETCHING_CONTACTS:
      return {
        ...state,
        meta: { isLoading: true }
      };
    case USER_LOGOUT:
      return INTIAL_STATE;
    default: {
      return state;
    }
  }
}
