import styled from 'styled-components';

const Button = styled.button`
  background-color: ${props => (props.backgroundColor || '#CCC')};
  color: ${props => (props.textColor || '#FFF')};
  border: 0;
  border-bottom: ${props => (`3px solid ${props.borderColor}` || 0)};
  padding: 8px;
  font-size: 0.9em;
  min-height: 25px;
`;

export default { Button };
