import React from 'react';
import styles from './index.styles';

export default (props) => {
  const { Button } = styles;
  return (
      <Button
        {...props}
      >
        {props.title}
      </Button>
  );
};
