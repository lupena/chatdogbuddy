import styled from 'styled-components';

const Input = styled.input`
  border-radius: 3px;
  border: 1px solid #b4b4b4;
  padding: 8px;
  display: block;
  min-height: 25px;
`;

export default { Input };
