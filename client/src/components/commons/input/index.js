import React from 'react';
import styles from './index.styles';

export default (props) => {
  const { Input } = styles;
  return (
      <Input
        {...props}
      />
  );
};
