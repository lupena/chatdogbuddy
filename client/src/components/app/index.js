import React, { Component } from 'react';
import Layout from './layout';
import Routes from '../../routes';

export default class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Routes />
        </Layout>
      </div>
    );
  }
}
