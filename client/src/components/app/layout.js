import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Header from '../header';
import styles from './layout.styles';

class Layout extends Component {
  render() {
    const { LayoutWrapper, ContentWrapper } = styles;
    const { pathname } = this.props.location;
    return (
      <LayoutWrapper>
        <Header pathname={pathname} onLogOut={() => this.props.history.push('/auth/login')} />
        <ContentWrapper>
          {this.props.children}
        </ContentWrapper>
      </LayoutWrapper>
    );
  }
}
const LayoutWithRouter = withRouter(props => <Layout {...props} />);
export default LayoutWithRouter;
