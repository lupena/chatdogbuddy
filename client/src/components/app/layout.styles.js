import styled from 'styled-components';

const LayoutWrapper = styled.div`
  font-family: 'Roboto', sans-serif;
`;

const ContentWrapper = styled.div`
  margin-top: 3em;
`;

export default { LayoutWrapper, ContentWrapper };
