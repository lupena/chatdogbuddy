import styled from 'styled-components';

const ThreadList = styled.section`
  max-width: 620px;
  margin: 0 auto;
  text-align: center;
`;

const Title = styled.h1`
  margin: 10px 0 30px 0;
  color: #5d585e;
  font-weight: bold;
`;

const MessageList = styled.div`
  list-style: none;
  text-align: left;
  padding: 0 10px;
  min-height: 120px;
  max-height: 280px;
  overflow: scroll;
`;

const MessageBubble = styled.div`
  background-color: #CCC;
`;

const MessageComposer = styled.textarea`
  min-height: 120px;
  font-size: 1.1em;
  width: 90%;
  margin: 0 auto;
  display: block;
  margin: 20px;
`;


export default { Title, MessageList, ThreadList, MessageBubble, MessageComposer };
