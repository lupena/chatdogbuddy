import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Button } from '../commons';
import MessageBubble from './messageBubble';
import { fetchMessages, submitMessage } from '../../actions';
import styles from './index.styles';
import { subscribeToThread, unSubscribeToThread, newMessageSent, onNewMessageReceived } from '../../socket';


export class Thread extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: ''
    };
  }
  componentWillMount() {
    const { match: { params } } = this.props;
    const recipientId = params.id;
    this.props.fetchMessages(recipientId);

    subscribeToThread(() => {
      this.props.fetchMessages(recipientId);
    });
    onNewMessageReceived(() => {
      this.props.fetchMessages(recipientId);
    });
  }
  componentDidMount() {
    this.scrollToBottom();
  }
  shouldComponentUpdate(nextProps) {
    if (!nextProps.currentUser.isAuth) return false;
    return true;
  }
  componentDidUpdate() {
    this.scrollToBottom();
  }
  componentWillUnMount() {
    const { match: { params } } = this.props;
    const recipientId = params.id;
    this.props.fetchMessages(recipientId);

    unSubscribeToThread(recipientId);
  }
  navigateBack() {
    window.history.back();
  }
  _handleKeyPress(e) {
    if (e.key === 'Enter') {
      this.sendMessage();
    }
  }
  sendMessage() {
    const { match: { params } } = this.props;
    const recipientId = params.id;
    const { message } = this.state;
    this.props.submitMessage(recipientId, message);
    setTimeout(() => { this.setState({ message: '' }); }, 100);
    newMessageSent(recipientId);
  }
  handleChange(event) {
    const key = event.target.id;
    const inputValue = event.target.value;
    const newState = { ...this.state };
    newState[key] = inputValue;
    this.setState(newState);
  }
  scrollToBottom = () => {
    const { messageList } = this;
    const scrollHeight = messageList.scrollHeight;
    const height = messageList.clientHeight;
    const maxScrollTop = scrollHeight - height;
    messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  }

  render() {
    const { currentUser, contacts, match: { params }, threads } = this.props;
    const { ThreadList, MessageList, Title, MessageComposer } = styles;

    const recipient = contacts.list.filter(c => (c._id === params.id));
    const recipientName = (recipient.length === 1) ? recipient[0].profile.name : 'Guest';

    const messages = threads.activeThread.messages.map(msg => (
      <MessageBubble
        key={msg._id}
        sender={recipientName}
        self={msg.author._id === currentUser._id}
        message={msg.body}
      />
    ));

    return (!currentUser.isAuth) ? <Redirect to='/auth/login' /> : (
      <div>
        <Button
          title="<< Back"
          textColor="#fe6e02"
          backgroundColor="transparent"
          onClick={this.navigateBack.bind(this)}
        />
        <ThreadList>
          <Title>
            Conversation With {recipientName}
          </Title>
          <MessageList innerRef={(messageList) => { this.messageList = messageList; }}>
            {messages}
          </MessageList>

          <MessageComposer
            placeholder="Write here your message..."
            autoFocus
            id="message"
            value={this.state.message}
            onChange={this.handleChange.bind(this)}
            onKeyPress={this._handleKeyPress.bind(this)}
          />
          <Button
            title="Send Message"
            backgroundColor="#fe6f00"
            borderColor="#fa5628"
            onClick={this.sendMessage.bind(this)}
          />

        </ThreadList>
      </div>
    );
  }
}

function mapStateToProps({ currentUser, contacts, threads }) {
  return { currentUser, contacts, threads };
}

export default compose(connect(mapStateToProps, { fetchMessages, submitMessage }))(Thread);
