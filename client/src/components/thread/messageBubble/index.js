import React from 'react';
import styles from './index.styles';

export default ({ sender, message, self = false }) => {
  const { MessageBubble } = styles;
  const senderLabel = (self) ? 'You' : sender;
  return (
    <div>
      <MessageBubble selfBubble={self}>
        <div>
          <span>{ senderLabel }</span>
        </div>
        <p>
          { message }
        </p>
      </MessageBubble>
    </div>
  );
};
