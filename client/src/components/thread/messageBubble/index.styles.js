import styled from 'styled-components';

const MessageBubble = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 10px;
    flex-direction: ${props => ((props.selfBubble) ? 'row-reverse' : 'initial')};
    div {
      flex: 0.2;
      text-align: center;
      span {
        position: relative;
        top: 30%;
        transform: translateY(-30%);
      }
    }
    p {
      padding: 10px;
      flex: 0.6;
      background-color: ${props => ((props.selfBubble) ? '#f7f7f7' : '#dee8e9')};
      margin: 0 10px;
      position: relative;
      border-radius: .4em;
    }
    p:after {
      content: '';
    	position: absolute;
    	top: 50%;
    	width: 0;
    	height: 0;
    	border: 20px solid transparent;
    	border-bottom: 0;
    	margin-top: -10px;
      right: ${props => ((props.selfBubble) ? 0 : null)};
      left: ${props => ((!props.selfBubble) ? 0 : null)};
      border-right: ${props => ((props.selfBubble) ? 0 : null)};
      border-left: ${props => ((!props.selfBubble) ? 0 : null)};
      margin-right: ${props => ((props.selfBubble) ? '-20px' : null)};
      margin-left: ${props => ((!props.selfBubble) ? '-20px' : null)};
      border-left-color: ${props => ((props.selfBubble) ? '#f7f7f7' : '#dee8e9')};
      border-right-color: ${props => ((!props.selfBubble) ? '#dee8e9' : '#f7f7f7')};
    }
  }
`;


export default { MessageBubble };
