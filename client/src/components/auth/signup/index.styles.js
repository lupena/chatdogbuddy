import styled from 'styled-components';

const Title = styled.h1`
  margin: 10px 0;
  color: #58c5ca;
`;

const SubTitle = styled.p`
  margin: 10px 0;
  color: #58565b;
`;

const SignUpForm = styled.form`
  max-width: 420px;
  margin: 0 auto;
  text-align: center;
   input {
     width: 70%;
     margin: 20px auto;
   }
   button {
     width: 50%;
     margin-top: 30px;
   }
`;


export default { Title, SubTitle, SignUpForm };
