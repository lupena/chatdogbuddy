import React from 'react';
import renderer from 'react-test-renderer';
import { Signup } from './index';

test('renders SignUp without crashing', () => {
  const currentUser = { meta: { isLoading: false } };
  const component = renderer.create(<Signup currentUser={currentUser} />)
  .toJSON();
  expect(component).toMatchSnapshot();
});
