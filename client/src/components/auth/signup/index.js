import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { signupAccount } from '../../../actions';
import styles from './index.styles';
import { Button, Input } from '../../commons';

export class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: ''
    };
  }
  onSignUpClick(e) {
    e.preventDefault();
    const { name, email, password } = this.state;
    const user = {
      name,
      email,
      password
    };
    this.props.signupAccount(user, () => {
      this.props.history.push('/messages');
    });
  }

  handleChange(event) {
    const key = event.target.id;
    const inputValue = event.target.value;
    const newState = { ...this.state };
    newState[key] = inputValue;
    this.setState(newState);
  }

  render() {
    const { currentUser } = this.props;
    const { isLoading } = currentUser.meta;

    if (isLoading) return <div>Cargando</div>;

    const { Title, SubTitle, SignUpForm } = styles;

    return (
      <div>
        <SignUpForm>
          <Title>
            Sign Up for BasketFriends!
          </Title>
          <SubTitle>
            Please enter your details
          </SubTitle>
          <Input
            type="text"
            name="name"
            id="name"
            placeholder="* Your Name"
            autoComplete="off"
            required
            autoFocus
            onChange={this.handleChange.bind(this)}
            value={this.state.username}
          />
          <Input
            type="email"
            name="email"
            id="email"
            placeholder="* Your Email Address"
            required
            onChange={this.handleChange.bind(this)}
            value={this.state.username}
          />
          <Input
            type="password"
            name="password"
            id="password"
            placeholder="* Your Password"
            required
            autoComplete="off"
            onChange={this.handleChange.bind(this)}
            value={this.state.password}
          />
          <Button
            name="Submit"
            value="Login"
            title="Sign Up"
            backgroundColor="#58c5ca"
            borderColor="#318c8f"

            onClick={this.onSignUpClick.bind(this)}
          />
        </SignUpForm>
      </div>
    );
  }
}

function mapStateToProps({ currentUser }) {
  return { currentUser };
}

export default compose(connect(mapStateToProps, { signupAccount }))(Signup);
