import styled from 'styled-components';

const Title = styled.h1`
  margin: 10px 0;
  color: #ff8e00;
  font-weight: bold;
`;

const SubTitle = styled.p`
  margin: 10px 0;
  color: #58565b;
  font-weight: bold;
`;

const LoginForm = styled.form`
  max-width: 320px;
  margin: 0 auto;
  text-align: center;
   input {
     width: 80%;
     margin: 20px auto;
   }
   button {
     width: 50%;
     margin-top: 30px;
   }
`;


export default { Title, SubTitle, LoginForm };
