import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { loginAccount } from '../../../actions';
import styles from './index.styles';
import { Button, Input } from '../../commons';

export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }
  onLoginClick(e) {
    e.preventDefault();
    this.props.loginAccount(this.state.username, this.state.password, () => {
      this.props.history.push('/messages');
    });
  }

  handleChange(event) {
    const key = event.target.id;
    const inputValue = event.target.value;
    const newState = { ...this.state };
    newState[key] = inputValue;
    this.setState(newState);
  }

  render() {
    const { currentUser } = this.props;
    const { isLoading } = currentUser.meta;

    const submitButton = (isLoading) ? <p>Loading</p> : (
      <Button
        title="Login"
        type="Submit"
        backgroundColor="#fe6f00"
        borderColor="#fa5628"
        onClick={this.onLoginClick.bind(this)}
      />
    );
    const { Title, SubTitle, LoginForm } = styles;

    return (
      <div>
        <LoginForm action="" method="post">
          <Title>
            Welcome Back!
          </Title>
          <SubTitle>
            Please Login
          </SubTitle>
          <Input
            type="email"
            id="username"
            name="username"
            placeholder="Enter email address"
            required
            autoFocus
            onChange={this.handleChange.bind(this)}
            value={this.state.username}
          />
          <Input
            type="password"
            name="password"
            id="password"
            placeholder="Enter password"
            required
            autoComplete="off"
            onChange={this.handleChange.bind(this)}
            value={this.state.password}
          />
          {submitButton}
        </LoginForm>
      </div>
    );
  }
}

function mapStateToProps({ currentUser }) {
  return { currentUser };
}

export default compose(connect(mapStateToProps, { loginAccount }))(Login);
