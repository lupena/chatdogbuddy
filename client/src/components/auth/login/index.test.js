import React from 'react';
import renderer from 'react-test-renderer';
import { Login } from './index';

test('renders Login without crashing', () => {
  const currentUser = { meta: { isLoading: false } };
  const component = renderer.create(<Login currentUser={currentUser} />)
  .toJSON();
  expect(component).toMatchSnapshot();
});
