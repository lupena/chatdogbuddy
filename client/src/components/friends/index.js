import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';
import styles from './index.styles';
import { Button } from '../commons';
import { fetchContacts } from '../../actions';

export class FriendList extends Component {
  componentWillMount() {
    this.props.fetchContacts();
  }

  shouldComponentUpdate(nextProps) {
    if (!nextProps.currentUser.isAuth) return false;
    return true;
  }
  openChat(friendId) {
    this.props.history.push(`/messages/${friendId}`);
  }
  render() {
    const { currentUser, contacts } = this.props;
    const { FriendsListWrapper, Friends, Title } = styles;

    const friends = contacts.list.map((contact) => (
      <li key={contact._id}>
        <span>{contact.profile.name}</span>
        <Button
          title="Send Message"
          backgroundColor="#fe6f00"
          borderColor="#fa5628"
          onClick={this.openChat.bind(this, contact._id)}
        />
      </li>
    ));

    return (!currentUser.isAuth) ? <Redirect to='/auth/login' /> :
    (
      <div>
        <FriendsListWrapper>
          <Title>
            Friends
          </Title>
          <Friends>
            {friends}
          </Friends>
        </FriendsListWrapper>
      </div>
    );
  }
}

function mapStateToProps({ currentUser, contacts }) {
  return { currentUser, contacts };
}

export default compose(connect(mapStateToProps, { fetchContacts }))(FriendList);
