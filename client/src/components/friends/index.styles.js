import styled from 'styled-components';

const FriendsListWrapper = styled.section`
  max-width: 420px;
  margin: 0 auto;
  text-align: center;
`;

const Title = styled.h1`
  margin: 10px 0 30px 0;
  color: #5d585e;
  font-weight: bold;
`;

const Friends = styled.ul`
  margin: 10px 0;
  padding: 0 10px;
  list-style: none;
  text-align: left;
  li {
    border: 2px solid #CCC;
    padding: 10px;
    overflow: hidden
  }
  li button {
    float: right;
  }
`;

export default { Title, Friends, FriendsListWrapper };
