import React from 'react';
import renderer from 'react-test-renderer';
import { FriendList } from './index';

test('renders FriendList without crashing', () => {
  const currentUser = { meta: { isLoading: false }, isAuth: true };
  const contacts = {
    list: [
      {
        profile: {
          name: 'omero'
        },
        _id: '5abbdf4af00c2c5a895d3031',
        email: 'osimpson@gmail.com'
      }
    ],
    meta: {
      isLoading: false
    }
  };
  const fetchContacts = jest.fn();
  const component = renderer.create(
    <FriendList
      fetchContacts={fetchContacts}
      currentUser={currentUser}
      contacts={contacts}
    />)
  .toJSON();
  expect(component).toMatchSnapshot();
});
