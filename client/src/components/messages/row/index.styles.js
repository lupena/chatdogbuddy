import styled from 'styled-components';

const MessageRow = styled.li`
    display: flex;
    flex-direction: row;
    border: 2px solid #CCC;
    margin-top: 10px;
    @media (max-width: 700px) {
      flex-direction: column;
    }
  div {
    padding: 10px;
  }
  div:nth-child(1) {
    flex: 0.3;
    p {
      display: ${props => ((props.unread) ? 'block' : 'none')};
      position: relative;
      top: -55px;
      left: -22px;
      font-size: 10px;
      background-color: #fe6e02;
      width: 25px;
      text-align: center;
      height: 25px;
      border-radius: 50%;
      color: #fff;
      line-height: 25px;
    }
  }
  div:nth-child(2) {
    flex: 0.5;
    background-color: #dee8e9;
    font-weight: ${props => ((props.unread) ? 'bold' : 'normal')};
    &:hover {
      cursor: pointer;
    }
  }
  div:nth-child(3) {
    flex: 0.2;
    background-color: #dee8e9;
    text-align: center;
    Button {
     width: 100%;
    }
    Button:hover {
      cursor: pointer;
    }
  }
`;

export default { MessageRow };
