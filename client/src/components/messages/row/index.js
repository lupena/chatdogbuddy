import React from 'react';
import styles from './index.styles';
import { Button } from '../../commons';

export default ({ sender, message, openChat, unread = false }) => {
  const { MessageRow } = styles;
  return (
    <MessageRow unread={unread}>
      <div>
        <span>{ sender }</span>
        <p>New</p>
      </div>
      <div onClick={() => { openChat(); }}>
        { message }
      </div>
      <div>
        <Button
          title="Reply"
          backgroundColor="#fe6f00"
          borderColor="#fa5628"
          onClick={() => { openChat(); }}
        />
      </div>
    </MessageRow>
  );
};
