import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import styles from './index.styles';
import MessageRow from './row';
import { fetchThreads } from '../../actions';
import { onNewMessageReceived } from '../../socket';

export class Messages extends Component {
  componentWillMount() {
    if (this.props.currentUser.isAuth) this.props.fetchThreads();
    onNewMessageReceived(() => {
      this.props.fetchThreads();
    });
  }
  shouldComponentUpdate(nextProps) {
    if (!nextProps.currentUser.isAuth) return false;
    return true;
  }

  openChat(friendId) {
    this.props.history.push(`/messages/${friendId}`);
  }

  messageUnread(t) {
    const { currentUser } = this.props;
    if (!('lastread' in t.thread)) return false;
    const read = t.thread.lastread.reduce((acc, curr) => {
      if (acc) return true;
      return (curr.user === currentUser._id && curr.read >= t.lastMessage.createdAt);
    }, false);
    return read;
  }

  render() {
    const { currentUser, threads } = this.props;
    const { MessagesListWrapper, MessageList, Title, NoMessages } = styles;

    if (!currentUser.isAuth) return <Redirect to='/auth/login' />;
    if (threads.list.length === 0) {
      return (
      <NoMessages>
        <h2>¿Why so shy?</h2>
        <h3>You haven't started a chat</h3>
        <a href='/friends'>
          Start a chat with your Friends.
        </a>
      </NoMessages>
      );
    }

    const messages = threads.list.map(t => {
      return (
        <MessageRow
          key={t.thread._id}
          sender={t.contact.profile.name}
          message={t.lastMessage.body}
          openChat={this.openChat.bind(this, t.contact._id)}
          unread={!(this.messageUnread(t))}
        />
      );
    });

    return (
      <div>
        <MessagesListWrapper>
          <Title>
            Messages
          </Title>
          <MessageList>
            {messages}
          </MessageList>
        </MessagesListWrapper>
      </div>
    );
  }
}

function mapStateToProps({ currentUser, threads }) {
  return { currentUser, threads };
}

export default compose(connect(mapStateToProps, { fetchThreads }))(Messages);
