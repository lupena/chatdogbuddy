import React from 'react';
import renderer from 'react-test-renderer';
import { Messages } from './index';

test('renders Messages without crashing', () => {
  const currentUser = { meta: { isLoading: false }, isAuth: true };
  const threads = { list: [] };
  const fetchThreads = jest.fn();
  const component = renderer.create(
    <Messages
      currentUser={currentUser}
      fetchThreads={fetchThreads}
      threads={threads}
    />)
  .toJSON();
  expect(component).toMatchSnapshot();
});
