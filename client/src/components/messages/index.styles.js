import styled from 'styled-components';

const MessagesListWrapper = styled.section`
  max-width: 520px;
  margin: 0 auto;
  text-align: center;
`;

const Title = styled.h1`
  margin: 10px 0 30px 0;
  color: #5d585e;
  font-weight: bold;
`;

const MessageList = styled.ul`
  list-style: none;
  text-align: left;
  padding: 0 10px;
`;

const NoMessages = styled.div`
  margin: 0 auto;
  text-align: center;
  padding: 30px;
  color: #5f5f5f;
`;


export default { Title, MessageList, MessagesListWrapper, NoMessages };
