import React from 'react';
import { Link } from 'react-router-dom';
import styles from './index.styles';

export default ({ pathname }) => {
  const { NavBar, MenuList, MenuLabel } = styles;

  return (
    <NavBar>
      <MenuList menuFloat='right'>
        <li>
          <Link to='/auth/login'>
            <MenuLabel current={(pathname === '/auth/login')}>
              Login
            </MenuLabel>
          </Link>
        </li>
        <li>
          <Link to='/auth/signup'>
            <MenuLabel current={(pathname === '/auth/signup')}>
              Sign Up
            </MenuLabel>
          </Link>
        </li>
      </MenuList>
    </NavBar>
  );
};
