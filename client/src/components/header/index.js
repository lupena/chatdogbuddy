import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { logoutAccount } from '../../actions';
import { Button } from '../commons';
import LoginMenu from './loginMenu';
import styles from './index.styles';

class Header extends Component {
  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutAccount();
    this.props.onLogOut();
  }
  render() {
    const { currentUser, pathname } = this.props;
    const { email, isAuth } = currentUser;
    if (!isAuth) return <LoginMenu pathname={pathname} />;
    const { NavBar, MenuList, MenuLabel } = styles;
    return (
      <NavBar background="#dddddf">
        <ThemeProvider theme={{ main: '#58ada8' }}>
        <MenuList menuFloat='left'>
          <li>
            <Link to='/messages'>
              <MenuLabel current={(pathname === '/messages')}>
                Messages
              </MenuLabel>
            </Link>
          </li>
          <li>
            <Link to='/friends'>
            <MenuLabel current={(pathname === '/friends')}>
              Friends
            </MenuLabel>
            </Link>
          </li>
        </MenuList>
        </ThemeProvider>
        <Button
          textColor="#656565"
          backgroundColor="transparent"
          title={`${email}, Logout`}
          onClick={this.onLogoutClick.bind(this)}
        />
      </NavBar>
    );
  }
}

function mapStateToProps({ currentUser }) {
  return { currentUser };
}

export default compose(connect(mapStateToProps, { logoutAccount }))(Header);
