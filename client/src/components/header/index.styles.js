import styled from 'styled-components';

const NavBar = styled.header`
  background-color: ${props => (props.background || '#FFF')};
  width: 100%;
  padding: 20px 0;
  color: #ffa500;
  overflow: hidden;
  button {
    float: right;
  }
  button:hover {
    cursor: pointer;
  }
`;

const MenuList = styled.ul`
  float: ${props => (props.menuFloat || 'right')};
  list-style: none;
  padding: 0;
  margin: 0;
  li { display: inline; }
  li a { text-decoration: none; }
`;

const MenuLabel = styled.div`
  float: left;
  margin: 0 15px;
  color: ${props => ((props.current) ? props.theme.main : '#000')};
`;

MenuLabel.defaultProps = {
  theme: {
    main: '#ff8e00'
  }
};


export default { NavBar, MenuList, MenuLabel };
