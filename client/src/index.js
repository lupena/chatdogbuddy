import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import registerServiceWorker from './registerServiceWorker';
import configureStore from './configureStore';
import App from './components/app';

const MainStore = configureStore();

ReactDOM.render(
  <Provider store={MainStore.store}>
    <PersistGate loading={null} persistor={MainStore.persistor}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </PersistGate>
  </Provider>
  , document.querySelector('#root'));
  registerServiceWorker();
