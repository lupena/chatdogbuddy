const express = require('express');

var app = express();
var cors = require('cors');
var port = process.env.PORT || 5000;
var mongoose = require('mongoose');
var passport = require('passport');
var passportService = require('./config/passport');
var bodyParser = require('body-parser');
var routes = require('./api/routes');
var config = require('./config');
var socketEvents = require('./socketEvents');
var http = require('http').Server(app);
var io = require('socket.io')(http);

mongoose.Promise = global.Promise;
mongoose.connect(config.database);

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(passport.initialize());

routes(app);
http.listen(port);
socketEvents(io);

console.log('Chat API server started on: ' + port);
