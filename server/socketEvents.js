exports = module.exports = function(io) {
  io.on('connection', (socket) => {

    socket.on('enter thread', (conversation) => {
      socket.join(conversation);
    });

    socket.on('leave thread', (conversation) => {
      socket.leave(conversation);
    })

    socket.on('new message', (msg) => {
      setTimeout(() => { io.emit('new message', msg); }, 1000);
    });
    
  });
}
