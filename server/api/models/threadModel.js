const mongoose = require('mongoose'),
      Schema = mongoose.Schema;

const ThreadSchema = new Schema({
  users: [
    { type: Schema.Types.ObjectId, ref: 'user'}
  ],
  lastUpdate: {
    type: Date
  },
  lastread: [
    {
      user: {
          type: Schema.Types.ObjectId , ref: 'user'
      },
      read: {
        type: Date
      }
    }
  ]
});

module.exports = mongoose.model('thread', ThreadSchema);
