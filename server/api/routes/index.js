'use strict';
module.exports = function(app) {
  const express = require('express'),
      auth = require('../controllers/authController'),
      ThreadController = require('../controllers/threadController');

  const apiRoutes = express.Router(),
        authRoutes = express.Router(),
        chatRoutes = express.Router();

  authRoutes.post('/signup', auth.register);
  authRoutes.post('/login', auth.requireLogin, auth.login);
  apiRoutes.use('/auth', authRoutes)

  chatRoutes.get('/', auth.requireAuth, ThreadController.getThreads);
  chatRoutes.get('/friends', auth.requireAuth, ThreadController.getFriends);
  chatRoutes.get('/:recipientId', auth.requireAuth, ThreadController.getMessages);
  chatRoutes.post('/:recipientId', auth.requireAuth, ThreadController.submitMessage);
  apiRoutes.use('/chat', chatRoutes);
  app.use(apiRoutes);

};
