"use strict"
const Thread = require('../models/threadModel'),
      Message = require('../models/messageModel'),
      User = require('../models/userModel');

exports.getThreads = function(req, res, next) {
  Thread.find({ users: req.user._id })
    .select('_id users lastread lastUpdate')
    .exec(function(err, threads) {
      if (err) {
        res.send({ error: err });
        return next(err);
      }

      let allThreads = [];
      threads.forEach(function(thread) {
        Message.find({ 'threadId': thread._id })
          .sort('-createdAt')
          .limit(1)
          .populate({
            path: "author",
            select: "profile.name"
          })
          .exec(function(err, message) {
            if (err) {
              res.send({ error: err });
              return next(err);
            }

            const contactId = (req.user._id.toString() === thread.users[0].toString()) ? thread.users[1] :
            thread.users[0];

            User.findOne({ _id: contactId }, '_id profile email', function(err, contact) {
              allThreads.push({ thread, lastMessage: message[0], contact });
              if(allThreads.length === threads.length) {
                return res.status(200).json({ threads: allThreads });
              }
            });
          });
      });
  });
}


exports.getMessages = function(req, res, next) {
  Thread.findOne({ users: { "$size" : 2, "$all": [ req.user._id.toString(), req.params.recipientId.toString() ] } }, '_id users lastUpdate lastread',
  function(err, currthread) {
    if (currthread === null) return res.status(200).json({ messages: [] });
    Message.find({ threadId: currthread._id })
      .select('createdAt body author')
      .sort('createdAt')
      .populate({
        path: 'author',
        select: 'profile.name'
      })
      .exec(function(err, messages) {
        if (err) {
          res.send({ error: err });
          return next(err);
        }
        let lastread = [];
        if (currthread.lastread) {
          lastread = currthread.lastread.filter(t => (t.user.toString() !== req.user._id.toString()));
        }
        lastread.push({ user: req.user._id, read: Date() })
        currthread.lastread = lastread;

        Thread.replaceOne({ _id: currthread._id }, currthread, { }, function(err, thread) {
            if (err)
              res.send(err);
            res.status(200).json({ messages });
        });
      });
  });
}


exports.submitMessage = function(req, res, next) {
  Thread.findOne({ users: { "$size" : 2, "$all": [ req.user._id.toString(), req.params.recipientId.toString() ] } }, '_id users unread',
    function(err, currthread) {
      if (currthread === null) {

        const newThread = new Thread({
          users: [req.user._id, req.params.recipientId]
        });
        newThread.save(function(err, thread) {
          if (err) {
            res.send({ error: err });
            return next(err);
          }
          return saveMessage(thread, req.body.message, req, res, next);
        });
      } else {
        return saveMessage(currthread, req.body.message, req, res, next);
      }
    });
}

const saveMessage = function(thread, message, req, res, next) {
  const newMessage = new Message({
    threadId: thread._id,
    body: message,
    author: req.user._id
  });

  newMessage.save(function(err, message) {
    if (err) {
      res.send({ error: err });
      return next(err);
    }

    res.status(200).json({ message: 'Reply successfully sent!' });
    return(next);
  });
}

exports.getFriends = function(req, res, next) {
  User.find({ _id: { $ne: req.user._id } }, '-password', function(err, users) {
    if (err)
      res.send(err);
    res.json(users);
  }).sort('profile.name');
}
