This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Requierements

- (REQUIRED) To run the server locally make sure you have nodemon & mongod installed globally:
```bash
npm i nodemon mongod -g
```

## First-time: Install dependencies
To install project dependencies (Server & Client) just run the following commands:
```bash
npm install
npm run install-project
```

## How to run the project
To run the project locally (Server & Client) just run the following command:
```bash
npm run start
```

## Chat with your coworkers
To be able to chat with your coworkers at least 2 users must signup (or just open 2 instances in diff browsers). For simplicity the friend list is populated with the users that have previously signed up for the App.
```bash
npm run start
```

## Running Test
```bash
npm run test
```

After running the command the web server opens http://localhost:3000/ and start the node server at http://localhost:5000/
